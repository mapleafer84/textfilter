#include "stdafx.h"
#include "FilterTreeView.h"
#include "TFCtxt.h"

BOOL CFilterTreeView::PreTranslateMessage(MSG* pMsg)
{
    pMsg;
    return FALSE;
}

LRESULT CFilterTreeView::OnLButtonDown(UINT, WPARAM, LPARAM lParam, BOOL & bHandled)
{
    POINT pt;
    pt.x = GET_X_LPARAM(lParam);
    pt.y = GET_Y_LPARAM(lParam);

    UINT uFlags;
    HTREEITEM hItem = HitTest(pt, &uFlags);
    if (hItem && (uFlags & TVHT_ONITEMSTATEICON))
    {
        LRESULT lRes = DefWindowProc();
        UpdateActiveFilterList();
        g_Ctxt.UpdateView(UPDATE_VIEW_CANVAS);
        bHandled = TRUE;
        return lRes;
    }

    bHandled = FALSE;
    return -1;

}

LRESULT CFilterTreeView::OnNewFilter(UINT, WPARAM wParam, LPARAM lParam, BOOL & bHandled)
{
    TFFilter *Filter = (TFFilter *)wParam;
    InsertFilter(Filter, Filter->m_Father->m_TreeItem, 0);

    return 0;
}

BOOL UpdateItemAction(CFilterTreeView *This, HTREEITEM Item)
{
    vector<TFFilter *> *FilterList = g_Ctxt.GetActiveFilterList();
    TFFilter *Filter = (TFFilter *)This->GetItemData(Item);
    if (Filter != NULL) {
        if ((Filter->m_FilterString != NULL) && This->GetCheckState(Item)) {
            FilterList->push_back(Filter);
        }
    }

    return FALSE;
}

void CFilterTreeView::UpdateActiveFilterList()
{
    vector<TFFilter *> *FilterList = g_Ctxt.GetActiveFilterList();

    FilterList->clear();
    ForeachItem(UpdateItemAction);
    g_Ctxt.UpdateViewLineSet();
}

BOOL ExpandAllAction(CFilterTreeView *This, HTREEITEM Item)
{
    This->Expand(Item);

    return FALSE;
}

int CFilterTreeView::ExpandAll()
{
    ForeachItem(ExpandAllAction);
    return 0;
}


int CFilterTreeView::ForeachItem(fnItemAction cbAction)
{
    HTREEITEM SiblingItem = GetRootItem();
    do {
        if (ForeachChild(SiblingItem, cbAction)) {
            break;
        }
    } while ((SiblingItem = GetNextSiblingItem(SiblingItem)) != NULL);
    return 0;
}


BOOL CFilterTreeView::ForeachChild(HTREEITEM Parent, fnItemAction cbAction)
{
    HTREEITEM ChildItem = Parent;
    if (cbAction(this, Parent)) {
        return TRUE;
    }

    ChildItem = GetChildItem(Parent);
    if (ChildItem != NULL) {
        do {
            if (ForeachChild(ChildItem, cbAction)) {
                return TRUE;
            }
        } while ((ChildItem = GetNextSiblingItem(ChildItem)) != NULL);
    }
    return FALSE;
}


int CFilterTreeView::ClearTreeNodes()
{
    HTREEITEM RootItem = NULL;

    while ((RootItem = GetRootItem()) != NULL) {
        DeleteItem(RootItem);
    }
    return 0;
}


void CFilterTreeView::AddGroup()
{
    HTREEITEM SelectedItem = GetSelectedItem();
    HTREEITEM NewItem = NULL;
    TFFilter *NewFilter = NULL;

    if (SelectedItem != NULL) {
        TFFilter *SelectedFilter = (TFFilter *)GetItemData(SelectedItem);
        if (SelectedFilter->m_FilterString == NULL) {
            NewFilter = new TFFilter("NewGroup");
            g_Ctxt.GetConfig()->AddFilter(SelectedFilter, NULL, NewFilter);

            NewItem = InsertItem(L"NewGroup", SelectedItem, NULL);
            BindNodeToFilter(NewItem, NewFilter);
            SelectItem(NewItem);
        }
    }
    else {
        NewFilter = new TFFilter("NewGroup");
        g_Ctxt.GetConfig()->AddFilter(NULL, NULL, NewFilter);

        NewItem = InsertItem(L"NewGroup", NULL, NULL);
        SetItemData(NewItem, (DWORD_PTR)NewFilter);
        SelectItem(NewItem);
    }
}


LRESULT CFilterTreeView::OnLButtonDblClk(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
    POINT pt;
    pt.x = GET_X_LPARAM(lParam);
    pt.y = GET_Y_LPARAM(lParam);

    UINT uFlags;
    HTREEITEM hItem = HitTest(pt, &uFlags);
    if (hItem && (uFlags & TVHT_ONITEMLABEL))
    {
        TFFilter *Filter = (TFFilter *)GetItemData(hItem);
        if (Filter != NULL && Filter->m_FilterString == NULL) {
            EditLabel(hItem);
        }
        bHandled = TRUE;
        return -1;
    }

    bHandled = FALSE;
    return -1;
}


void CFilterTreeView::BindNodeToFilter(HTREEITEM TreeNode, TFFilter * Filter)
{
    SetItemData(TreeNode, (DWORD_PTR)Filter);
    Filter->m_TreeItem = TreeNode;
}


HTREEITEM CFilterTreeView::InsertFilter(TFFilter * Filter, HTREEITEM Parent, HTREEITEM After)
{
    int NodeNameLen = strlen(Filter->m_NodeName);
    int NodeWNameLen = 0;
    TCHAR *NodeName = NULL;
    HTREEITEM TreeNode = NULL;
    NodeWNameLen = MultiByteToWideChar(CP_ACP, 0, Filter->m_NodeName, NodeNameLen, NodeName, 0);
    if (NodeWNameLen > 0) {
        NodeName = new TCHAR[NodeWNameLen + 1];
        MultiByteToWideChar(CP_ACP, 0, Filter->m_NodeName, NodeNameLen, NodeName, NodeWNameLen);
        NodeName[NodeWNameLen] = 0;
        TreeNode = InsertItem(NodeName, Parent, NULL);
        BindNodeToFilter(TreeNode, Filter);
        delete[] NodeName;
    }

    return TreeNode;
}
