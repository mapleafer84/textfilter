#include "stdafx.h"
#include "TFFind.h" 
#include "TFCtxt.h"

inline bool kmp_next(LPBYTE sub_str, int length, vector<int> &next)
{
    next.resize(length);
    next[0] = 0;
    int temp;

    for (int i = 1; i<next.size(); i++)
    {
        temp = next[i - 1];
        while (sub_str[i] != sub_str[temp] && temp>0)
        {
            temp = next[temp - 1];
        }
        if (sub_str[i] == sub_str[temp])
            next[i] = temp + 1;
        else next[i] = 0;
    }
    return true;
}

bool kmp_search(LPBYTE line_text
    , int line_length
    , LPBYTE sub_str
    , int sub_str_length
    , vector<int> &next
    , int &pos)
{
    int tp = 0;
    int mp = 0;


    for (tp = 0; tp<line_length; tp++)
    {
        while (line_text[tp] != sub_str[mp] && mp) {
            mp = next[mp - 1];
        }

        if (line_text[tp] == sub_str[mp])
            mp++;
        if (mp == sub_str_length)
        {
            pos = tp - mp + 1;
            return true;
        }
    }

    if (tp == line_length)
        return false;

    return false;
}

int line_find_by_substr(TFLineDataInfo *line, LPBYTE sub_str, int length)
{
    LPBYTE line_data = g_Ctxt.GetDatabase();
    int npos = -1;
    vector<int> next;
    kmp_next(sub_str, length, next);

    kmp_search(&line_data[line->Start], line->Length, sub_str, length, next, npos);

    return npos;
}

int line_find_by_substr(TFLineSet *line_set, unsigned int start, LPBYTE sub_str, int length)
{
    TFLineDataInfo *line_info = NULL;
    int npos = -1;
    bool found = false;

    if ((line_set == NULL) || (line_set->m_Lines.size() == 0)
        || (sub_str == NULL) || (length == 0)
        || (start > line_set->m_Lines.size() - 1)) {
        return -1;
    }

    vector<TFLineDataInfo *>::iterator it;
    for (it = line_set->m_Lines.begin() + start; it != line_set->m_Lines.end(); it++) {
        line_info = *it;
        npos = line_find_by_substr(line_info, sub_str, length);
        if (npos != -1) {
            found = true;
            break;
        }
    }

    if (found) {
        return it - line_set->m_Lines.begin();
    }

    return -1;
}

int line_find_by_substr(TFLineSet *lines, LPBYTE sub_str, int length, TFLineSet *res_lines)
{
    size_t index = 0;
    LPBYTE line_data = g_Ctxt.GetDatabase();
    vector<int> next;
    kmp_next(sub_str, length, next);

    res_lines->m_Data = lines->m_Data;

    for (index = 0; index < lines->m_Lines.size(); index++) {
        int npos = -1;
        TFLineDataInfo *line = lines->m_Lines.at(index);
        if (kmp_search(&line_data[line->Start], line->Length, sub_str, length, next, npos)) {
            res_lines->m_Lines.push_back(line);
            res_lines->m_MaxLineLength = max(res_lines->m_MaxLineLength, line->Length);
        }
    }

    return res_lines->m_Lines.size();
}

static bool _line_find_by_no(TFLineSet *line_set, unsigned int line_no, unsigned int &near_prev, unsigned int &near_next)
{
    unsigned int top, low, cur;
    bool found = false;
    TFLineDataInfo *line_info = NULL;

    if ((line_set->m_Lines.size() == 0)) {
        near_next = -1;
        near_prev = -1;
        return false;
    }

    low = 0;
    top = line_set->m_Lines.size() - 1;

    do {
        if (low + 1 >= top) {
            if (line_set->m_Lines.at(low)->LineNo == line_no) {
                found = true;
                near_next = near_prev = low;
            }
            else if (line_set->m_Lines.at(top)->LineNo == line_no)
            {
                found = true;
                near_next = near_prev = top;
            }
            break;
        }
        else {
            cur = (low + top) / 2;
            line_info = line_set->m_Lines.at(cur);

            if (line_no > line_info->LineNo) {
                low = cur;
            }
            else if (line_no < line_info->LineNo) {
                top = cur;
            }
            else {
                found = true;
                near_next = near_prev = cur;
                break;
            }
            cur = (top + low) / 2;
        }
    } while (top > low);

    if (!found) {
        if (line_no > line_set->m_Lines.at(top)->LineNo) {
            low = top;
            top = -1;
        }
        if (line_no < line_set->m_Lines.at(low)->LineNo) {
            top = low;
            low = -1;
        }
        near_prev = low;
        near_next = top;
    }
    return found;
}

unsigned int line_find_by_no(TFLineSet *line_set, unsigned int line_no)
{
    int index = -1;
    unsigned int near_prev, near_next;
    if (_line_find_by_no(line_set, line_no, near_prev, near_next)) {
        ATLASSERT(near_prev == near_next);
        index = near_prev;
    }
    return index;
}

unsigned int line_find_near_prev_by_no(TFLineSet *line_set, unsigned int line_no)
{
    unsigned int near_prev, near_next;
    _line_find_by_no(line_set, line_no, near_prev, near_next);
    return near_prev;
}

unsigned int line_find_near_next_by_no(TFLineSet *line_set, unsigned int line_no)
{
    unsigned int near_prev, near_next;
    _line_find_by_no(line_set, line_no, near_prev, near_next);
    return near_next;
}