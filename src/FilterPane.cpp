#include "stdafx.h"
#include "resource.h"
#include "FilterPane.h"

void CFilterPane::UpdateLayout()
{
    RECT rcClient = { 0 };
    GetClientRect(&rcClient);
    UpdateLayout(rcClient.right, rcClient.bottom);
}

void CFilterPane::UpdateLayout(int cxWidth, int cyHeight)
{
    ATLASSERT(::IsWindow(m_hWnd));
    RECT rect = { 0 };

    if (IsVertical())
    {
        ::SetRect(&rect, 0, 0, m_cxyHeader, cyHeight);
        if (m_tb.m_hWnd != NULL)
            m_tb.SetWindowPos(NULL, m_cxyBorder, m_cxyBorder + m_cxyBtnOffset, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE);

        if (m_wndClient.m_hWnd != NULL)
            m_wndClient.SetWindowPos(NULL, m_cxyHeader, 0, cxWidth - m_cxyHeader, cyHeight, SWP_NOZORDER);
        else
            rect.right = cxWidth;
    }
    else
    {
        ::SetRect(&rect, 0, 0, cxWidth, m_cxyHeader);
        if (m_tb.m_hWnd != NULL)
            m_tb.SetWindowPos(NULL, rect.left + 100, m_cxyBorder + m_cxyBtnOffset, 100, 20, SWP_NOZORDER);

        if (m_wndClient.m_hWnd != NULL)
            m_wndClient.SetWindowPos(NULL, 0, m_cxyHeader, cxWidth, cyHeight - m_cxyHeader, SWP_NOZORDER);
        else
            rect.bottom = cyHeight;
    }

    InvalidateRect(&rect);
}

void CFilterPane::DrawButtonImage(CDCHandle dc, RECT& rcImage, HPEN hPen)
{

}

LRESULT CFilterPane::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    TBBUTTON tbbtn = { 0 };
    CImageList imgList;
    imgList.CreateFromImage(IDR_PANE_TOOLBAR, 16, 1, CLR_NONE, IMAGE_BITMAP,
        LR_LOADTRANSPARENT | LR_LOADMAP3DCOLORS | LR_CREATEDIBSECTION);

    m_tb.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | CCS_NODIVIDER | CCS_NORESIZE | CCS_NOPARENTALIGN | CCS_NOMOVEY | TBSTYLE_TOOLTIPS | TBSTYLE_FLAT, 0);
    m_tb.SetButtonStructSize();

    m_tb.SetBitmapSize(m_cxImageTB, m_cyImageTB);
    m_tb.SetButtonSize(m_cxImageTB + m_cxyBtnAddTB, m_cyImageTB + m_cxyBtnAddTB);

    m_tb.SetImageList(imgList);

    tbbtn.fsState = TBSTATE_ENABLED;
    tbbtn.fsStyle = BTNS_BUTTON;

    tbbtn.idCommand = ID_TFC_OPEN;
    tbbtn.iBitmap = 0;
    m_tb.AddButton(&tbbtn);

    tbbtn.iBitmap++;
    tbbtn.idCommand = ID_TFC_SAVE;
    m_tb.AddButton(&tbbtn);

    tbbtn.iBitmap++;
    tbbtn.idCommand = FILTER_PANE_ADDGRP_CMDID;
    m_tb.AddButton(&tbbtn);

    tbbtn.iBitmap++;
    tbbtn.idCommand = FILTER_PANE_DELGRP_CMDID;
    m_tb.AddButton(&tbbtn);

    CalcSize();
    return 0;
}
