#pragma once
#include <vector>
#include "TFFile.h"

using namespace std;
int line_find_by_substr(TFLineDataInfo *line, LPBYTE sub_str, int length);
int line_find_by_substr(TFLineSet *lines, LPBYTE sub_str, int length, TFLineSet *res_lines);
int line_find_by_substr(TFLineSet *line_set, unsigned int start, LPBYTE sub_str, int length);
unsigned int line_find_by_no(TFLineSet *line_set, unsigned int line_no);
unsigned int line_find_near_prev_by_no(TFLineSet *line_set, unsigned int line_no);
unsigned int line_find_near_next_by_no(TFLineSet *line_set, unsigned int line_no);


