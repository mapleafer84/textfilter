// textfilterView.cpp : implementation of the CTextfilterView class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "ColorComboBox.h"
#include "textfilterView.h"
#include "TFCtxt.h"
#include "FilterDlg.h"

BOOL CTextfilterView::PreTranslateMessage(MSG* pMsg)
{
	pMsg;

	return FALSE;
}

void CTextfilterView::UpdateScrollInfo()
{
    SCROLLINFO si;
    memset(&si, 0, sizeof(si));
    unsigned int LineCount = g_Ctxt.GetLineCount();
    unsigned int LineMaxLength = g_Ctxt.GetLineMaxLength();

    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_RANGE | SIF_PAGE;
    si.nPage = 1;
    si.nMin = 0;
    si.nMax = (LineCount > 0) ? (LineCount - 1) : 0;
    SetScrollInfo(SB_VERT, &si);

    memset(&si, 0, sizeof(si));
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_RANGE | SIF_PAGE;
    si.nPage = 1;
    si.nMin = 0;
    si.nMax = (LineMaxLength > 0) ? (LineMaxLength - 1) : 0;
    SetScrollInfo(SB_HORZ, &si);
}
int CTextfilterView::CaculateScroll(int Scrol, WPARAM wParam)
{
    int NewPos = 0;
    SCROLLINFO si;
    memset(&si, 0, sizeof(si));
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_ALL;
    GetScrollInfo(Scrol, &si);

    NewPos = si.nPos;

    switch (LOWORD(wParam)) {
    case SB_LEFT:
        NewPos = si.nMin;
        break;
    case SB_RIGHT:
        NewPos = si.nMax;
        break;
    case SB_PAGEUP:
        NewPos -= si.nPage;
        break;
    case SB_PAGEDOWN:
        NewPos += si.nPage;
        break;
    case SB_LINEUP:
        NewPos -= 1;
        break;
    case SB_LINEDOWN:
        NewPos += 1;
        break;
    case SB_THUMBPOSITION:
        NewPos = si.nTrackPos;
        break;
    case SB_THUMBTRACK:
        NewPos = si.nTrackPos;
        break;
    default:
        break;
    }

    NewPos = max(si.nMin, NewPos);
    NewPos = min(NewPos, si.nMax);

    return NewPos;
}

void CTextfilterView::ScrollToChar(int Char)
{
    SCROLLINFO si;

    memset(&si, 0, sizeof(si));
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_ALL;
    GetScrollInfo(SB_HORZ, &si);

    si.nPos = Char;

    si.fMask = SIF_POS;
    SetScrollInfo(SB_HORZ, &si, TRUE);
    GetScrollInfo(SB_HORZ, &si);

    g_Ctxt.SetViewCharOffset(si.nPos);
    Invalidate();
}

void CTextfilterView::ScrollToLine(int Line)
{
    SCROLLINFO si;

    memset(&si, 0, sizeof(si));
    si.cbSize = sizeof(SCROLLINFO);
    si.fMask = SIF_ALL;
    GetScrollInfo(SB_VERT, &si);

    si.nPos = Line;
    si.nPage = 10;
    si.fMask = SIF_POS | SIF_PAGE;
    SetScrollInfo(SB_VERT, &si, TRUE);
    GetScrollInfo(SB_VERT, &si);

    g_Ctxt.SetViewLineOffset(si.nPos, FALSE);
    Invalidate();
}


LRESULT CTextfilterView::OnMouseWheel(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    int fwKeys = LOWORD(wParam);// 指出是否有CTRL、SHIFT、鼠标键(左、中、右、附加)按下，允许复合
    int zDelta = (short)HIWORD(wParam);// zDelta传递滚轮滚动的快慢，该值小于零表示滚轮向后滚动	
    if (fwKeys & MK_CONTROL) {
        if (zDelta > 0) {
            g_Ctxt.UpdateFontRatio(true);
        }
        else if (zDelta < 0) {
            g_Ctxt.UpdateFontRatio(false);
        }
    }
    else {
        int FirstLineInView = g_Ctxt.GetViewLineOffset();

        if (zDelta < 0) {
            FirstLineInView += g_Ctxt.GetLineCountInView() / 3;
        }
        else if (zDelta > 0) {
            FirstLineInView -= g_Ctxt.GetLineCountInView() / 3;
        }

        ScrollToLine(FirstLineInView);
        return 0;
    }

    bHandled = FALSE;
    return 0;
}

LRESULT CTextfilterView::OnUpdateScrollPos(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    ScrollToLine(g_Ctxt.GetViewLineOffset());

    return 0;
}

LRESULT CTextfilterView::OnUpdateScroll(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    UpdateScrollInfo();
    Invalidate();
    return 0;
}


LRESULT CTextfilterView::OnEraseBkgnd(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    bHandled = TRUE;
    return 0;
}


LRESULT CTextfilterView::OnLButtonDblClk(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    int yPos = GET_Y_LPARAM(lParam);
    int SelectLine = g_Ctxt.GetLineFromY(yPos);
    CFilterDlg dlgFilter;
    TFFilter *Filter = NULL;

    if (SelectLine != -1) {
        LPBYTE Database = g_Ctxt.GetDatabase();
        TFLineDataInfo *LineDataInfo = g_Ctxt.GetLineDataInfo(SelectLine);
        int Length = LineDataInfo->Length;

        Filter = new TFFilter;

        Filter->m_FilterString = new char[Length + 1];
        memcpy(Filter->m_FilterString, Database + LineDataInfo->Start, Length);
        Filter->m_FilterString[Length] = 0;

        dlgFilter.SetFilter(Filter);
        if (dlgFilter.DoModal() == IDOK) {
            g_Ctxt.NotifyTreeViewNewFilter(Filter);
        }
        else {
            delete Filter;
        }
    }

    return 0;
}


LRESULT CTextfilterView::OnLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    int yPos = GET_Y_LPARAM(lParam);
    int SelectLine = g_Ctxt.GetLineFromY(yPos);

    if (SelectLine != -1) {
        g_Ctxt.SetHotLine(SelectLine, TRUE);
    }
    return 0;
}


LRESULT CTextfilterView::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    return 0;
}

LRESULT CTextfilterView::OnHScroll(UINT, WPARAM wParam, LPARAM, BOOL &)
{
    int NewPos = CaculateScroll(SB_HORZ, wParam);
    ScrollToChar(NewPos);

    Invalidate();
    return 0;
}

LRESULT CTextfilterView::OnVScroll(UINT, WPARAM wParam, LPARAM, BOOL &)
{
    int NewPos = CaculateScroll(SB_VERT, wParam);

    ScrollToLine(NewPos);

    return 0;
}


LRESULT CTextfilterView::OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    PAINTSTRUCT ps;
    HDC dc;
    HDC memDc;
    RECT bmRect;
    CBitmap bm;

    dc = BeginPaint(&ps);

    GetClientRect(&bmRect);

    memDc = CreateCompatibleDC(dc);
    bm.CreateCompatibleBitmap(dc
        , bmRect.right - bmRect.left
        , bmRect.bottom - bmRect.top);

    SelectObject(memDc, bm.m_hBitmap);

    g_Ctxt.UpdateViewCanvas(memDc);

    BitBlt(dc, 0, 0, bmRect.right - bmRect.left, bmRect.bottom - bmRect.top, memDc, 0, 0, SRCCOPY);

    DeleteDC(memDc);
    bm.DeleteObject();

    EndPaint(&ps);

    return 0;
}
