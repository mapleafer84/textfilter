#include "stdafx.h"
#include <atlmisc.h>
#include "ColorComboBox.h"
#include "TFCtxt.h"


CColorComboBox::CColorComboBox()
{

}

CColorComboBox::~CColorComboBox()
{

}

void CColorComboBox::InitColorList()
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    for (int i = 0; i < g_Ctxt.GetColorListSize(); i++) {
        AddString(L"Sample text");
    }

    SetCurSel(0);
}

BOOL CColorComboBox::OnEraseBkgnd(CDCHandle dc)
{
    return TRUE;
}

void CColorComboBox::OnPaint(CDCHandle)
{
    int Index = GetCurSel();
    CPaintDC dc(m_hWnd);    CRect rc;    GetClientRect(&rc);    TCHAR str[60];    GetWindowText(str, 60);    Index = (Index >= 0) ? Index : 0;    if (CString(str).IsEmpty() == FALSE) {        dc.SetBkMode(TRANSPARENT);        dc.SetTextColor(g_Ctxt.GetColor(Index));        rc.left += 5;        dc.DrawText(str, -1, &rc, DT_VCENTER | DT_SINGLELINE | DT_LEFT);    }}

void CColorComboBox::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpdis)
{
    int index = lpdis->itemID;    CDCHandle dc(lpdis->hDC);    CRect rc = lpdis->rcItem;    TCHAR sz[60] = { 0 };    if (index >= 0) {        GetLBText(index, sz);        dc.SetTextColor(g_Ctxt.GetColor(index));        dc.SetBkMode(TRANSPARENT);        rc.left += 5;        dc.DrawText(sz, -1, &rc, DT_VCENTER | DT_SINGLELINE | DT_LEFT);    }}