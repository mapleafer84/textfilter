#include "stdafx.h"
#include "TFFile.h"

TFFile g_TxtFile;

TFFile::TFFile() : m_Loaded(0)
    , m_FileHandle(INVALID_HANDLE_VALUE)
    , m_MapHandle(NULL)
{
}

int TFFile::LoadFile(TCHAR * FileName)
{
    LPBYTE Data = NULL;

    m_FileHandle = CreateFile(FileName
        , GENERIC_READ
        , FILE_SHARE_READ
        , NULL
        , OPEN_EXISTING
        , FILE_ATTRIBUTE_NORMAL
        , NULL);

    if (m_FileHandle == INVALID_HANDLE_VALUE) {
        goto ERR_RET;
    }

    m_Length = GetFileSize(m_FileHandle, NULL);

    m_MapHandle = CreateFileMapping(m_FileHandle, NULL, PAGE_READONLY, 0, 0, NULL);
    if (m_MapHandle == NULL) {
        goto ERR_RET;
    }

    Data = (LPBYTE)MapViewOfFile(m_MapHandle, FILE_MAP_READ, 0, 0, 0);
    if (Data == NULL) {
        goto ERR_RET;
    }

    m_LineSet.m_Data = Data;
    m_Loaded = 1;
    return 0;
ERR_RET:
    UnloadFile();
    return -1;
}

int TFFile::InitLineData()
{
    UINT32 Start = 0;
    UINT32 End = Start;
    UINT32 LineNo = 1;
    LPBYTE Data = m_LineSet.m_Data;

#define STATE_WAIT_LINE_END 0
#define STATE_WAIT_LINE_START 1

    int State = STATE_WAIT_LINE_END;

    if (m_Loaded == 0) {
        goto ERR_RET;
    }

    for (int i = 0; i < m_Length; i++) {
        switch (State) {
        case STATE_WAIT_LINE_END:
            if (Data[i] == '\n' || Data[i] == '\r') {
                TFLineDataInfo *LineData = new TFLineDataInfo;
                LineData->Start = Start;
                LineData->Length = End - Start + 1;
                LineData->LineNo = LineNo++;
           
                m_LineSet.m_Lines.push_back(LineData);
                m_LineSet.m_MaxLineLength = max(m_LineSet.m_MaxLineLength, LineData->Length);
                State = STATE_WAIT_LINE_START;
            }
            else {
                End++;
            }
            break;

        case STATE_WAIT_LINE_START:
            if (Data[i] != '\n' && Data[i] != '\r') {
                Start = i;
                End = Start;

                State = STATE_WAIT_LINE_END;
            }
            break;
        }
    }

    if ((End != Start) && (State != STATE_WAIT_LINE_START)) {
        TFLineDataInfo *LineData = new TFLineDataInfo;
        LineData->Start = Start;
        LineData->Length = End - Start + 1;
        LineData->LineNo = LineNo++;
        m_LineSet.m_Lines.push_back(LineData);
        m_LineSet.m_MaxLineLength = max(m_LineSet.m_MaxLineLength, LineData->Length);
    }

    return 0;
ERR_RET:
    return -1;
}

TFLineSet * TFFile::GetLineSet()
{
    return &m_LineSet;
}

LPBYTE TFFile::GetDataBase()
{
    return m_LineSet.m_Data;
}


int TFFile::UnloadFile()
{
    if (m_LineSet.m_Data != NULL) {
        UnmapViewOfFile(m_LineSet.m_Data);
        m_LineSet.m_Data = NULL;
    }
    if (m_MapHandle != NULL) {
        CloseHandle(m_MapHandle);
        m_MapHandle = NULL;
    }

    if (m_FileHandle != INVALID_HANDLE_VALUE) {
        CloseHandle(m_FileHandle);
        m_FileHandle = INVALID_HANDLE_VALUE;
    }

    for (int i = 0; i < m_LineSet.m_Lines.size(); i++) {
        TFLineDataInfo *LineData = m_LineSet.m_Lines.at(i);
        delete LineData;
    }

    m_LineSet.m_Lines.clear();
    m_LineSet.m_MaxLineLength = 0;

    m_Loaded = 0;
    return 0;
}


bool TFFile::IsLoaded()
{
    return m_Loaded;
}
