#include "stdafx.h"
#include "TFCtxt.h"
#include "TFFind.h"
#include <list>
using namespace std;

TFCtxt g_Ctxt;
TFFilter::TFFilter() : m_FilterString(NULL)
    , m_Father(NULL)
    , m_NodeName(NULL)
    , m_FilterStringLength(0)
    , m_LineSet(NULL)
    , m_TreeItem(NULL)
{
}

TFFilter::TFFilter(char *NodeName) : m_FilterString(NULL)
    , m_Father(NULL)
    , m_FilterStringLength(0)
    , m_LineSet(NULL)
{
    m_NodeName = new char[strlen(NodeName) + 1];
    strcpy(m_NodeName, NodeName);
}

TFFilter::~TFFilter()
{
    if (m_FilterString != NULL) {
        delete[] m_FilterString;
        m_FilterString = NULL;
    }

    if (m_NodeName != NULL) {
        delete[] m_NodeName;
        m_NodeName = NULL;
    }

    for (int i = 0; i < m_Children.size(); i++) {
        TFFilter *Filter = m_Children.at(i);
        delete Filter;
    }

    if (m_LineSet != NULL) {
        delete m_LineSet;
        m_LineSet = NULL;
    }

    m_TreeItem = NULL;
    m_Children.clear();
}

TFCtxt::TFCtxt()
    : m_DefaultFontWidth(8)
    , m_DefaultFontHeight(16)
    , m_FontRatio(1.0)
    , m_ViewOffsetX(0)
    , m_ViewOffsetY(0)
    , m_HotLine(0)
    , m_FindStart(-1)
    , m_MatchedLine(-1)
{
    m_FontHeight = m_DefaultFontHeight;
    m_FontWidth = m_DefaultFontWidth;

    m_BkBrush = CreateSolidBrush(0xFFFFFF);
    m_HotLineBrush = CreateSolidBrush(0xC1FFC1);
    m_VLinePen = CreatePen(PS_SOLID, 1, 0x808080);

    m_ColorSet.push_back(RGB(0x00, 0x00, 0x00));
    m_ColorSet.push_back(RGB(238, 18, 137));
    m_ColorSet.push_back(RGB(139, 58, 98));
    m_ColorSet.push_back(RGB(238, 0, 0));
    m_ColorSet.push_back(RGB(205, 102, 0));
    m_ColorSet.push_back(RGB(132, 112, 255));
    m_ColorSet.push_back(RGB(205, 129, 98));
    m_ColorSet.push_back(RGB(148, 0, 211));
    m_ColorSet.push_back(RGB(107, 142, 35));
    m_ColorSet.push_back(RGB(32, 178, 170));
    m_ColorSet.push_back(RGB(0, 191, 255));

    UpdateFont();
}


TFCtxt::~TFCtxt()
{
}


void TFCtxt::UpdateFontRatio(bool Inc)
{
    float Ratio = m_FontRatio;
    if (Inc) {
        Ratio += 0.2;
    }
    else {
        Ratio -= 0.2;
    }

    if (Ratio < 0.5) {
        Ratio = 0.5;
    }

    if (Ratio > 3.0) {
        Ratio = 3.0;
    }

    m_FontRatio = Ratio;
    m_FontWidth = m_DefaultFontWidth * m_FontRatio;
    m_FontHeight = m_DefaultFontHeight * m_FontRatio;

    UpdateFont();
    UpdateView(0);
}


int TFCtxt::GetLineHeight()
{
    return m_FontHeight + 1;
}


void TFCtxt::AttachView(HWND hWnd)
{
    m_AttachView = hWnd;
}

HFONT TFCtxt::GetFont(void)
{
    return m_Font;
}
void TFCtxt::UpdateFont()
{
    LOGFONT logFont;
    memset(&logFont, 0, sizeof(logFont));
    // �����С
    logFont.lfHeight = m_FontHeight;
    logFont.lfWidth = 0;
    logFont.lfEscapement = 0;
    logFont.lfOrientation = 0;
    logFont.lfWeight = 500;
    logFont.lfItalic = 0;
    logFont.lfUnderline = 0;
    logFont.lfStrikeOut = 0;
    logFont.lfCharSet = ANSI_CHARSET;
    logFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
    logFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
    logFont.lfQuality = PROOF_QUALITY;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
    lstrcpy(logFont.lfFaceName, _T("Consolas"));

    if (m_Font != NULL) {
        DeleteObject(m_Font);
    }
    m_Font = CreateFontIndirect(&logFont);
}


int TFCtxt::LoadFromFile(TCHAR * FileName)
{
    if (m_File.IsLoaded()) {
        m_File.UnloadFile();
    }

    if (m_File.LoadFile(FileName) == 0) {
        m_File.InitLineData();
        ClearActiveFilterLineSet();
        UpdateViewLineSet();
        return 0;
    }

    return -1;
}


void TFCtxt::UpdateView(UINT32 Target)
{
    if (Target == UPDATE_VIEW_CANVAS) {
        InvalidateRect(m_AttachView, NULL, TRUE);
    }
    else if (Target == UPDATE_VIEW_SCROLL) {
        SendMessage(m_AttachView, WM_UPDATE_SCROLL, 0, 0);
    }
    else if (Target == UPDATE_VIEW_SCROLL_POS) {
        SendMessage(m_AttachView, WM_UPDATE_SCROLL_POS, 0, 0);
    }
}

int TFCtxt::CalcLineNoWidth()
{
    TFLineDataInfo *LineDataInfo;
    int LineNo = 0;
    int LineNoWidth = 1;

    if (m_ViewOffsetY < GetLineCount()) {
        LineDataInfo = m_ViewLineSet.m_Lines[m_ViewOffsetY];
        LineNo = LineDataInfo->LineNo;
    }

    do {
        LineNo /= 10;
        LineNoWidth++;
    } while (LineNo);

    return LineNoWidth * g_Ctxt.GetFontWidth();
}

void TFCtxt::UpdateViewCanvas(HDC hDc)
{
    if (m_AttachView != NULL) {
        HFONT oldFont = (HFONT)SelectObject(hDc, GetFont());
        TFLineDataInfo *LineDataInfo = NULL;
        RECT ViewRect;
        RECT HotlineRect;
        int LinePos = 0;
        int CharPos = 0;
        int TotalLineCount = m_ViewLineSet.m_Lines.size();
        int X, Y, LineNoWidth, LineNoHeight;

        GetClientRect(m_AttachView, &ViewRect);

        FillRect(hDc, &ViewRect, m_BkBrush);
        SetBkMode(hDc, TRANSPARENT);

        // Draw seperate line
        HPEN DefaultPen = (HPEN)SelectObject(hDc, m_VLinePen);
        LineNoWidth = CalcLineNoWidth();
        LineNoHeight = (g_Ctxt.m_ViewLineSet.m_Lines.size() - m_ViewOffsetY);
        LineNoHeight = min(LineNoHeight, GetLineCountInView());
        LineNoHeight *= GetLineHeight();
        MoveToEx(hDc, LineNoWidth + m_FontWidth + m_FontWidth / 2, 0, NULL);
        LineTo(hDc, LineNoWidth + m_FontWidth + m_FontWidth / 2, LineNoHeight);
        SelectObject(hDc, DefaultPen);

        //Draw line text
        Y = 0;
        for (LinePos = m_ViewOffsetY; LinePos < TotalLineCount; LinePos++) {
            X = LineNoWidth;
            LineDataInfo = m_ViewLineSet.m_Lines.at(LinePos);

            //Draw Line NO
            size_t LineNoTemp = LineDataInfo->LineNo;
            SetTextColor(hDc, 0);
            do {
                TCHAR ch = LineNoTemp % 10 + L'0';
                TextOut(hDc, X, Y, &ch, 1);
                X -= m_FontWidth;
                LineNoTemp /= 10;
            } while (LineNoTemp);

            X = LineNoWidth + m_FontWidth * 2;

            if (LinePos == m_MatchedLine) {
                HotlineRect.left = X;
                HotlineRect.right = ViewRect.right;
                HotlineRect.top = (LinePos - m_ViewOffsetY) * GetLineHeight() + 1;
                HotlineRect.bottom = HotlineRect.top + GetLineHeight() - 1;
                FillRect(hDc, &HotlineRect, m_HotLineBrush);
            }

            if (LinePos == m_HotLine) {
                MoveToEx(hDc, X, Y + GetLineHeight(), NULL);
                LineTo(hDc, ViewRect.right, Y + GetLineHeight());
            }

            SetTextColor(hDc, GetColor(LineDataInfo->Color));

            for (CharPos = m_ViewOffsetX; CharPos < LineDataInfo->Length; CharPos++) {
                TCHAR Char = m_ViewLineSet.m_Data[LineDataInfo->Start + CharPos];
                TextOut(hDc, X, Y, &Char, 1);

                X += m_FontWidth;
                if (X > ViewRect.right) {
                    break;
                }
            }

            Y += GetLineHeight();
            if (Y > ViewRect.bottom) {
                break;
            }
        }
        SelectObject(hDc, oldFont);
    }
}

int TFCtxt::GetFontWidth()
{
    return m_FontWidth;
}
void TFCtxt::SetViewCharOffset(int Pos)
{
    m_ViewOffsetX = Pos;
}

void TFCtxt::SetViewLineOffset(int Pos, BOOL Update)
{
    if (Pos < m_ViewLineSet.m_Lines.size()) {
        m_ViewOffsetY = Pos;

        if (Update) {
            UpdateView(UPDATE_VIEW_SCROLL_POS);
        }
    }
}
int TFCtxt::UpdateViewLineSet()
{
    UINT32 MiniLineNo = 0xFFFFFFFF;
    int MiniFilterIndex = -1;
    int TotalLineCount;
    UINT32 MaxLineLength = 0;
    TFFilter *Filter;
    vector<int> IndexSet;
    TFLineSet *FileLineSet = m_File.GetLineSet();
    TFLineDataInfo *LineDataInfo = NULL;
    TFLineDataInfo *MiniLineNoDataInfo = NULL;

    m_ViewLineSet.m_Data = m_File.GetDataBase();
    m_ViewLineSet.m_Lines.clear();

    if (!m_File.IsLoaded()) {
        return 0;
    }
    /* Update LineSet by filter*/
    if (m_ActiveFilters.size() == 0) {
        for (int i = 0; i < FileLineSet->m_Lines.size(); i++) {
            LineDataInfo = FileLineSet->m_Lines[i];
            m_ViewLineSet.m_Lines.push_back(LineDataInfo);
        }
        m_ViewLineSet.m_MaxLineLength = FileLineSet->m_MaxLineLength;
    }
    else {
        for (int i = 0; i < m_ActiveFilters.size(); i++) {
            Filter = m_ActiveFilters.at(i);
            if (Filter->m_LineSet == NULL) {
                Filter->m_LineSet = new TFLineSet;
                line_find_by_substr(FileLineSet, (LPBYTE)Filter->m_FilterString, Filter->m_FilterStringLength, Filter->m_LineSet);
            }
            IndexSet.push_back(0);
        }

        /*Merge the result*/
        do {
            MiniLineNo = 0xFFFFFFFF;
            MiniLineNoDataInfo = NULL;
            MiniFilterIndex = -1;

            for (int i = 0; i < m_ActiveFilters.size(); i++) {
                Filter = m_ActiveFilters.at(i);
                if (IndexSet[i] == Filter->m_LineSet->m_Lines.size()) {
                    continue;
                }
                LineDataInfo = Filter->m_LineSet->m_Lines[IndexSet[i]];
                if (MiniLineNo > LineDataInfo->LineNo) {
                    MiniLineNo = LineDataInfo->LineNo;
                    MiniFilterIndex = i;
                    MiniLineNoDataInfo = LineDataInfo;
                    MiniLineNoDataInfo->Color = Filter->m_Color;
                }
            }

            if (MiniLineNoDataInfo != NULL) {
                for (int i = 0; i < m_ActiveFilters.size(); i++) {
                    Filter = m_ActiveFilters[i];
                    if (IndexSet[i] == Filter->m_LineSet->m_Lines.size()) {
                        continue;
                    }
                    if (Filter->m_LineSet->m_Lines[IndexSet[i]]->LineNo == MiniLineNoDataInfo->LineNo) {
                        IndexSet[i]++;
                    }
                }
                m_ViewLineSet.m_Lines.push_back(MiniLineNoDataInfo);
            }
        } while (MiniLineNo != 0xFFFFFFFF);
    }
    UpdateView(UPDATE_VIEW_SCROLL);
    UpdateView(UPDATE_VIEW_CANVAS);

    return 0;
}

int TFCtxt::GetLineCount()
{
    return m_ViewLineSet.m_Lines.size();
}

int TFCtxt::GetLineMaxLength()
{
    return m_ViewLineSet.m_MaxLineLength;
}

int TFCtxt::GetLineCountInView()
{
    RECT Rect;
    GetClientRect(m_AttachView, &Rect);

    return (Rect.bottom - Rect.top) / GetLineHeight();
}

int TFCtxt::GetViewLineOffset()
{
    return m_ViewOffsetY;
}

bool TFCtxt::IsMatchAnyFilter(LPBYTE Data, TFLineDataInfo * LineDataInfo)
{
    TFFilter *Filter = NULL;
    BOOL IsMatch = FALSE;

    if (m_ActiveFilters.size() == 0) {
        return TRUE;
    }

    for (int i = 0; i < m_ActiveFilters.size(); i++) {
        Filter = m_ActiveFilters.at(i);
        if (line_find_by_substr(LineDataInfo, (LPBYTE)Filter->m_FilterString, Filter->m_FilterStringLength) > 0) {
            IsMatch = TRUE;
            break;
        }
    }
    return IsMatch;
}


void TFCtxt::SetHotLine(int LineNo, BOOL Update)
{
    m_HotLine = LineNo;
    m_FindStart = LineNo;
    m_MatchedLine = -1;

    if (Update) {
        UpdateView(UPDATE_VIEW_CANVAS);
    }
}


int TFCtxt::GetFirstLine()
{
    return m_ViewOffsetY;
}


int TFCtxt::GetLineFromY(int Y)
{
    int LineNo = GetFirstLine();
    LineNo += Y / GetLineHeight();

    if (LineNo < GetLineCount()) {
        return LineNo;
    }

    return -1;
}


TFLineDataInfo * TFCtxt::GetLineDataInfo(int LineNo)
{
    if (LineNo < GetLineCount()) {
        return m_ViewLineSet.m_Lines.at(LineNo);
    }
    return nullptr;
}


LPBYTE TFCtxt::GetDatabase()
{
    return m_ViewLineSet.m_Data;
}

int TFCtxt::LoadTfc(TCHAR * FileName)
{
    return m_Config.LoadTfc(FileName);
}

TFConfig::TFConfig() :m_Loaded(FALSE)
    , m_FileName(NULL)
{
}

TFConfig::~TFConfig()
{
}

int TFConfig::LoadTfc(TCHAR * FileName)
{
    DWORD Length;
    LPBYTE Data = NULL;
    HANDLE FileHandle = INVALID_HANDLE_VALUE;
    HANDLE MapHandle = NULL;
    char LineText[1024 * 4];

    if (m_Loaded) {
        UnloadTfc();
    }

    FileHandle = CreateFile(FileName
        , GENERIC_READ
        , FILE_SHARE_READ
        , NULL
        , OPEN_EXISTING
        , FILE_ATTRIBUTE_NORMAL
        , NULL);

    if (FileHandle == INVALID_HANDLE_VALUE) {
        goto ERR_RET;
    }

    
    Length = GetFileSize(FileHandle, NULL);

    MapHandle = CreateFileMapping(FileHandle, NULL, PAGE_READONLY, 0, 0, NULL);
    if (MapHandle == NULL) {
        goto ERR_RET;
    }

    Data = (LPBYTE)MapViewOfFile(MapHandle, FILE_MAP_READ, 0, 0, 0);
    if (Data == NULL) {
        goto ERR_RET;
    }

    Initialize((char *)Data, Length);

    SetFileName(FileName);

ERR_RET:
    if (Data != NULL) {
        UnmapViewOfFile(Data);
    }

    if (MapHandle != NULL) {
        CloseHandle(MapHandle);
    }

    if (FileHandle != INVALID_HANDLE_VALUE) {
        CloseHandle(FileHandle);
    }

    if (!m_Loaded) {
        UnloadTfc();
    }

    return m_Loaded == TRUE ? 0 : -1;
}


void TFConfig::UnloadTfc()
{
    for (int i = 0; i < m_RootNode.m_Children.size(); i++) {
        TFFilter *Filter = m_RootNode.m_Children.at(i);
        delete Filter;
    }

    m_RootNode.m_Children.clear();

    if (m_FileName != NULL) {
        delete[] m_FileName;
        m_FileName = NULL;
    }

    m_Loaded = FALSE;
}


int TFConfig::Initialize(char* Data, DWORD Length)
{
    char *LineStart;
    char *LineEnd;
    DWORD LineLength;

    LineStart = Data;

    m_RootNode.m_Father = NULL;
    m_RootNode.m_FilterString = NULL;
    m_RootNode.m_NodeName = new char[2];
    m_RootNode.m_NodeName[0] = '/';
    m_RootNode.m_NodeName[1] = 0;

    do {
        TFFilter *Filter = NULL;
        LineEnd = strstr(LineStart, "\n");
        LineLength = (LineEnd != NULL) ? (LineEnd - LineStart) : (Length - (LineStart - Data));

        InitFilter(LineStart, LineLength);

        if (LineEnd != NULL) {
            LineStart = LineEnd + 1;
        }
    } while (LineEnd != NULL);

    return 0;
}


TFFilter *TFConfig::InitFilter(char * CfgString, DWORD Length)
{
    TFFilter *Filter = NULL;
    TFFilter *FatherNode = NULL;
    char *CfgTmp = new char[Length + 1];
    char *ParseStart = NULL;
    char *ParseEnd = NULL;
    char *ParseTmp = NULL;
    memcpy(CfgTmp, CfgString, Length);
    CfgTmp[Length] = 0;

    ParseStart = CfgTmp + 1;
    ParseEnd = strstr(CfgTmp, "]");
    if (ParseEnd == NULL) {
        goto ERR_RET;
    }

    *ParseEnd = 0;
    FatherNode = &m_RootNode;

    /*Parse [/xxx/xxx]*/
    do {
        ParseTmp = strstr(ParseStart, "/");
        if (ParseTmp > ParseStart) {
            *ParseTmp = 0;
        }

        if (ParseTmp != ParseStart) {
            Filter = FindFilterNode(FatherNode, ParseStart);
            if (Filter == NULL) {
                Filter = new TFFilter;
                Filter->m_Father = FatherNode;
                FatherNode->m_Children.push_back(Filter);
                Filter->m_NodeName = new char[strlen(ParseStart) + 1];
                strcpy(Filter->m_NodeName, ParseStart);
            }
            ParseStart += strlen(ParseStart) + 1;
            FatherNode = Filter;
        }

        if (ParseTmp == ParseStart) {
            ParseStart = ParseTmp + 1;
        }
    } while (ParseTmp != NULL);

    FatherNode = Filter;
    Filter = new TFFilter;
    Filter->m_Father = FatherNode;

    /*Parse tfc_tag*/
    ParseStart = strstr(ParseStart, "tfc_tag");
    if (ParseStart == NULL) {
        goto ERR_RET;
    }
    Filter->m_NodeName = GetValueFromString(ParseStart + strlen("tfc_tag"));
    if (Filter->m_NodeName == NULL) {
        goto ERR_RET;
    }

    ParseStart = strstr(ParseStart, "tfc_filter");
    if (ParseStart == NULL) {
        goto ERR_RET;
    }
    Filter->m_FilterString = GetValueFromString(ParseStart + strlen("tfc_filter"));
    if (Filter->m_FilterString == NULL) {
        goto ERR_RET;
    }
    else {
        Filter->m_FilterStringLength = strlen(Filter->m_FilterString);
    }

    ParseStart = strstr(ParseStart, "tfc_color");
    if (ParseStart == NULL) {
        goto ERR_RET;
    }
    char *ColorString = GetValueFromString(ParseStart + strlen("tfc_color"));
    if (ColorString == NULL) {
        goto ERR_RET;
    }
    sscanf_s(ColorString, "%d", &Filter->m_Color);
    delete[] ColorString;
    FatherNode->m_Children.push_back(Filter);
    return Filter;
ERR_RET:
    if (Filter != NULL) {
        delete Filter;
    }
    return NULL;
}


TFFilter * TFConfig::FindFilterNode(TFFilter *FatherNode, char * NodeName)
{
    TFFilter *FilterNode = NULL;
    for (int i = 0; i < FatherNode->m_Children.size(); i++) {
        FilterNode = FatherNode->m_Children.at(i);
        if (strcmp(FilterNode->m_NodeName, NodeName) == 0) {
            return FilterNode;
        }
    }
    return NULL;
}


char * TFConfig::GetValueFromString(char * String)
{
#define PARSE_WAIT_EQUAL        0
#define PARSE_WAIT_LEFT_QUETO   1
#define PARSE_WAIT_RIGHT_QUETO   2
#define PARSE_ESC               3
#define PARSE_DONE               4

    int StringLen = strlen(String);
    char *ParsedString = NULL;
    int State = PARSE_WAIT_EQUAL;
    char *Temp = new char[StringLen];
    int TempLen = 0;
   
    for (int i = 0; i < StringLen; i++) {
        switch (State) {
        case PARSE_WAIT_EQUAL:
            if (String[i] == ' ') {
                continue;
            }
            else if (String[i] == '=') {
                State = PARSE_WAIT_LEFT_QUETO;
            }
            else {
                goto ERR_RET;
            }
            break;
        case PARSE_WAIT_LEFT_QUETO:
            if (String[i] == ' ') {
                continue;
            }
            else if (String[i] == '\"') {
                State = PARSE_WAIT_RIGHT_QUETO;
            }
            else {
                goto ERR_RET;
            }
            break;
        case PARSE_WAIT_RIGHT_QUETO:
            if (String[i] == '\\') {
                State = PARSE_ESC;
            }
            else if (String[i] == '\"') {
                Temp[TempLen++] = 0;
                State = PARSE_DONE;
                goto DONE;
            }
            else {
                Temp[TempLen++] = String[i];
            }
            break;
        case PARSE_ESC:
            if (String[i] == '\\') {
                Temp[TempLen++] = '\\';
                State = PARSE_WAIT_RIGHT_QUETO;
            }
            else if (String[i] == '\"') {
                Temp[TempLen++] = '\"';
                State = PARSE_WAIT_RIGHT_QUETO;
            }
            else {
                goto ERR_RET;
            }
            break;
        }
    }

DONE:
    if (State == PARSE_DONE) {
        ParsedString = new char[TempLen];
        strcpy(ParsedString, Temp);
    }
    
ERR_RET:
    delete[] Temp;
    return ParsedString;
}


bool TFConfig::IsLoaded()
{
    return m_Loaded;
}


TFConfig * TFCtxt::GetConfig()
{
    return &m_Config;
}

vector<TFFilter*>* TFCtxt::GetActiveFilterList()
{
    return &m_ActiveFilters;
}


TFFilter * TFConfig::GetRootNode()
{
    return &m_RootNode;
}


void TFCtxt::ClearActiveFilterLineSet()
{
    for (int i = 0; i < m_ActiveFilters.size(); i++) {
        TFFilter *Filter = m_ActiveFilters[i];
        if (Filter->m_LineSet != NULL) {
            delete Filter->m_LineSet;
            Filter->m_LineSet = NULL;
        }
    }
}


void TFConfig::AddFilter(TFFilter * Father, TFFilter * After, TFFilter * NewFilter)
{
    if (Father != NULL) {
        NewFilter->m_Father = Father;
        Father->m_Children.push_back(NewFilter);
    }
    else {
        NewFilter->m_Father = &m_RootNode;
        m_RootNode.m_Children.push_back(NewFilter);
    }
}


void TFFilter::SetNodeName(TCHAR * NodeName)
{
    int NodeWNameLen = 0;
    int NodeNameLen = 0;
    if (NodeName != NULL) {
        NodeWNameLen = wcslen(NodeName);
        if (m_NodeName != NULL) {
            delete[] m_NodeName;
            m_NodeName = NULL;
        }

        NodeNameLen = WideCharToMultiByte(CP_ACP, 0, NodeName, NodeWNameLen, m_NodeName, 0, NULL, NULL);
        m_NodeName = new char[NodeNameLen + 1];

        WideCharToMultiByte(CP_ACP, 0, NodeName, NodeWNameLen, m_NodeName, NodeNameLen, NULL, NULL);
        m_NodeName[NodeNameLen] = 0;
    }
}

int TFCtxt::SaveTfc(TCHAR * FileName)
{
    return GetConfig()->SaveTfc(FileName);
}

void TFCtxt::NotifyTreeViewNewFilter(TFFilter * NewFilter)
{
    SendMessage(m_TreeView, WM_TREEVIEW_NEW_FILTER, (WPARAM)NewFilter, 0);
}


int TFConfig::SaveTfc(TCHAR * FileName)
{
    TFFilter *FatherNode = &m_RootNode;
    HANDLE FileHandle = CreateFile(FileName
        , GENERIC_WRITE
        , FILE_SHARE_READ
        , NULL
        , CREATE_ALWAYS
        , FILE_ATTRIBUTE_NORMAL
        , NULL);

    if (FileHandle == INVALID_HANDLE_VALUE) {
        goto ERR_RET;
    }

    if (SaveTfcChildren(FileHandle, FatherNode) == 0) {

    }

    SetFileName(FileName);

    CloseHandle(FileHandle);
    return 0;
ERR_RET:
    CloseHandle(FileHandle);
    return -1;
}


int TFConfig::SaveTfcChildren(HANDLE FileHandle, TFFilter * FatherNode)
{
    if (FatherNode->m_Children.size() == 0) {
        return 0;
    }
    else {
        for (int i = 0; i < FatherNode->m_Children.size(); i++) {
            TFFilter *Father;
            TFFilter *Filter = FatherNode->m_Children[i];
            if (SaveTfcChildren(FileHandle, Filter) == 0) {
                list<TFFilter *>PathList;

                Father = Filter;
                while (Father->m_Father != NULL) {
                    if (Father->m_FilterString == NULL) {
                        PathList.push_back(Father);
                    }
                    Father = Father->m_Father;
                }

                WriteFile(FileHandle, "[", 1, NULL, NULL);
                while (PathList.size()) {
                    Father = PathList.back();
                    WriteFile(FileHandle, "/", 1, NULL, NULL);
                    WriteFile(FileHandle, Father->m_NodeName, strlen(Father->m_NodeName), NULL, NULL);
                    PathList.pop_back();
                }
                WriteFile(FileHandle, "]", 1, NULL, NULL);
                if (Filter->m_FilterString != NULL) {
                    char szColor[16];
                    WriteFile(FileHandle, "tfc_tag", strlen("tfc_tag"), NULL, NULL);
                    WriteFile(FileHandle, "=", 1, NULL, NULL);
                    WriteFile(FileHandle, "\"", 1, NULL, NULL);
                    WriteFile(FileHandle, Filter->m_NodeName, strlen(Filter->m_NodeName), NULL, NULL);
                    WriteFile(FileHandle, "\" ", 2, NULL, NULL);

                    WriteFile(FileHandle, "tfc_filter", strlen("tfc_filter"), NULL, NULL);
                    WriteFile(FileHandle, "=", 1, NULL, NULL);
                    WriteFile(FileHandle, "\"", 1, NULL, NULL);
                    WriteFile(FileHandle, Filter->m_FilterString, strlen(Filter->m_FilterString), NULL, NULL);
                    WriteFile(FileHandle, "\" ", 2, NULL, NULL);

                    WriteFile(FileHandle, "tfc_color", strlen("tfc_color"), NULL, NULL);
                    WriteFile(FileHandle, "=", 1, NULL, NULL);
                    WriteFile(FileHandle, "\"", 1, NULL, NULL);
                    sprintf(szColor, "%d", Filter->m_Color);
                    WriteFile(FileHandle, szColor, strlen(szColor), NULL, NULL);
                    WriteFile(FileHandle, "\"", 1, NULL, NULL);
                }
                WriteFile(FileHandle, "\n", 1, NULL, NULL);
            }
        }
    }
    return FatherNode->m_Children.size();
}


int TFConfig::SetFileName(TCHAR * FileName)
{
    if (m_FileName != NULL) {
        delete[] m_FileName;
        m_FileName = NULL;
    }

    m_FileName = new TCHAR[wcslen(FileName) + 1];
    wcscpy(m_FileName, FileName);

    m_Loaded = TRUE;

    return 0;
}


TCHAR * TFConfig::GetFileName()
{
    return m_FileName;
}


int TFFilter::SetFilterString(TCHAR * FilterString)
{
    int FilterStringWLen = 0;

    if (FilterString != NULL) {
        FilterStringWLen = _tcslen(FilterString);

        if (m_FilterString != NULL) {
            delete[] m_FilterString;
            m_FilterString = NULL;
        }

        m_FilterStringLength = 0;

        m_FilterStringLength = WideCharToMultiByte(CP_ACP, 0, FilterString, FilterStringWLen, m_FilterString, 0, NULL, NULL);
        m_FilterString = new char[m_FilterStringLength + 1];

        WideCharToMultiByte(CP_ACP, 0, FilterString, FilterStringWLen, m_FilterString, m_FilterStringLength, NULL, NULL);
        m_FilterString[m_FilterStringLength] = 0;
    }
    return 0;
}


void TFFilter::SetFather(TFFilter * Father)
{
    m_Father = Father;
    m_Father->m_Children.push_back(this);
}


void TFFilter::SetColorIndex(int Index)
{
    m_Color = Index;
}


void TFCtxt::SetTreeView(HWND TreeView)
{
    m_TreeView = TreeView;
}

void TFCtxt::FindReset(void)
{
    m_FindStart = 0;
    m_MatchedLine = -1;
    UpdateView(UPDATE_VIEW_CANVAS);
}

int TFCtxt::FindLastLine(char * szKey, int Length)
{
    int Found = FALSE;
    int FindTmp;
    TFLineDataInfo *LineDataInfo = NULL;

    if (m_FindStart == -1) {
        m_FindStart = 0;
    }

    if (m_MatchedLine == -1) {
        FindTmp = m_FindStart;
    }
    else if (m_MatchedLine > 0) {
        FindTmp = m_MatchedLine - 1;
    }
    else {
        FindTmp = m_ViewLineSet.m_Lines.size() - 1;
    }

    do {
        if (FindTmp < m_ViewLineSet.m_Lines.size()) {
            LineDataInfo = m_ViewLineSet.m_Lines.at(FindTmp);
            if (line_find_by_substr(LineDataInfo, (LPBYTE)szKey, Length) > 0) {
                m_MatchedLine = FindTmp;
                Found = TRUE;
                break;
            }
            FindTmp--;
            if (FindTmp == -1) {
                FindTmp = m_ViewLineSet.m_Lines.size() - 1;
            }
        }
    } while (FindTmp != m_FindStart);

    if (Found) {
        if (m_MatchedLine < m_ViewOffsetY) {
            m_ViewOffsetY = m_MatchedLine;
        }

        if (m_MatchedLine >= m_ViewOffsetY + GetLineCountInView()) {
            m_ViewOffsetY = m_MatchedLine;
        }
        UpdateView(UPDATE_VIEW_SCROLL_POS);
    }
    else {
        m_MatchedLine = -1;
    }
    return m_MatchedLine;
}

int TFCtxt::FindNextLine(char * szKey, int Length)
{
    int Found = FALSE;
    int FindTmp;
    TFLineDataInfo *LineDataInfo = NULL;

    if (m_FindStart == -1) {
        m_FindStart = 0;
    }

    FindTmp = (m_MatchedLine == -1) ? m_FindStart : m_MatchedLine + 1;

    do {
        if (FindTmp < m_ViewLineSet.m_Lines.size()) {
            LineDataInfo = m_ViewLineSet.m_Lines.at(FindTmp);
            if (line_find_by_substr(LineDataInfo, (LPBYTE)szKey, Length) > 0) {
                m_MatchedLine = FindTmp;
                Found = TRUE;
                break;
            }
            FindTmp = (FindTmp + 1) % m_ViewLineSet.m_Lines.size();
        }
    } while (FindTmp != m_FindStart);

    if (Found) {
        if (m_MatchedLine < m_ViewOffsetY) {
            m_ViewOffsetY = m_MatchedLine;
        }

        if (m_MatchedLine >= m_ViewOffsetY + GetLineCountInView()) {
            m_ViewOffsetY = m_MatchedLine;
        }
        UpdateView(UPDATE_VIEW_SCROLL_POS);
    }
    else {
        m_MatchedLine = -1;
    }
    return m_MatchedLine;
}


COLORREF TFCtxt::GetColor(int Index)
{
    if (Index >= m_ColorSet.size()) {
        Index = 0;
    }

    return m_ColorSet.at(Index);
}


int TFCtxt::GetColorListSize()
{
    return m_ColorSet.size();
}
