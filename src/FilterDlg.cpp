#include "stdafx.h"
#include "resource.h"
#include <atlmisc.h>
#include "TFCtxt.h"
#include "ColorComboBox.h"
#include "FilterDlg.h"

LRESULT CFilterDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    SetDlgItemTextA(m_hWnd, IDC_EDIT_FILTER_STRING, (LPCSTR)m_Filter->m_FilterString);
    m_GroupCtrl.Attach(GetDlgItem(IDC_COMBO_GROUP));
    m_ColorCtrl.SubclassWindow(GetDlgItem(IDC_COMBO_COLOR));

    InitGroupList();
    m_ColorCtrl.InitColorList();

    CenterWindow(GetParent());
    return 0;
}

LRESULT CFilterDlg::OnBnClickedOk(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加控件通知处理程序代码
    TFFilter *Father = NULL;
    TCHAR Text[1024 + 1];

    GetDlgItemText(IDC_EDIT_TAG, Text, 1024);
    if (_tcslen(Text) == 0) {
        MessageBox(TEXT("Tag editbox is empty!"));
        goto ERR_RET;
    }

    GetDlgItemText(IDC_EDIT_FILTER_STRING, Text, 1024);
    if (_tcslen(Text) == 0) {
        MessageBox(TEXT("Filter string editbox is empty!"));
        goto ERR_RET;
    }

    if (m_GroupCtrl.GetCurSel() == CB_ERR) {
        MessageBox(TEXT("Group is empty!"));
        goto ERR_RET;
    }
    Father = (TFFilter *)m_GroupCtrl.GetItemData(m_GroupCtrl.GetCurSel());

    m_Filter->SetFather(Father);

    GetDlgItemText(IDC_EDIT_FILTER_STRING, Text, 1024);
    m_Filter->SetFilterString(Text);

    GetDlgItemText(IDC_EDIT_TAG, Text, 1024);
    m_Filter->SetNodeName(Text);

    m_Filter->SetColorIndex(m_ColorCtrl.GetCurSel());

    EndDialog(wID);
ERR_RET:
    return wID;
}


void CFilterDlg::SetFilter(TFFilter * Filter)
{
    m_Filter = Filter;
}


void CFilterDlg::InitGroupList()
{
    TFFilter *RootFilter = g_Ctxt.GetConfig()->GetRootNode();
    InitItemFromFilter(RootFilter);
    m_GroupCtrl.SetCurSel(0);
}


void CFilterDlg::InitItemFromFilter(TFFilter * Father)
{
    TFFilter *Filter = NULL;

    if ((Father != NULL) && Father->m_FilterString == NULL) {
        int NewIndex = ::SendMessageA(m_GroupCtrl.m_hWnd, CB_ADDSTRING, 0, (LPARAM)Father->m_NodeName);
        m_GroupCtrl.SetItemData(NewIndex, (DWORD_PTR)Father);
    }
    for (int i = 0; i < Father->m_Children.size(); i++) {
        Filter = Father->m_Children[i];
        InitItemFromFilter(Filter);
    }
}

LRESULT CFilterDlg::OnBnClickedCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加控件通知处理程序代码
    EndDialog(wID);
    return 0;
}
