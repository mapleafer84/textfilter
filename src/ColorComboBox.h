#pragma once
#include <atlcrack.h>
class CColorComboBox : public CWindowImpl<CColorComboBox, CComboBox>
    , public COwnerDraw<CColorComboBox>
{
public:
    CColorComboBox();
    ~CColorComboBox();
    BOOL OnEraseBkgnd(CDCHandle dc);
    void OnPaint(CDCHandle);
    void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpdis);
    void InitColorList();

    BEGIN_MSG_MAP_EX(CColorComboBox)
        MSG_WM_ERASEBKGND(OnEraseBkgnd)
        MSG_WM_PAINT(OnPaint)
        MSG_WM_DRAWITEM(OnDrawItem)
        CHAIN_MSG_MAP_ALT(COwnerDraw<CColorComboBox>, 1)
        DEFAULT_REFLECTION_HANDLER()
    END_MSG_MAP()
};

