#pragma once
class CFilterTreeView;
#include "TFCtxt.h"

typedef BOOL (*fnItemAction)(CFilterTreeView *This, HTREEITEM Item);

class CFilterTreeView : public CWindowImpl<CFilterTreeView, CTreeViewCtrl>
{
public:
    //DECLARE_WND_CLASS(NULL)
    DECLARE_WND_SUPERCLASS(NULL, CTreeViewCtrl::GetWndClassName())

    BOOL PreTranslateMessage(MSG* pMsg);
    LRESULT OnLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
    LRESULT OnNewFilter(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);

    BEGIN_MSG_MAP(CFilterTreeView)
        MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
        MESSAGE_HANDLER(WM_TREEVIEW_NEW_FILTER, OnNewFilter)
        DEFAULT_REFLECTION_HANDLER()
        MESSAGE_HANDLER(WM_LBUTTONDBLCLK, OnLButtonDblClk)
    END_MSG_MAP()
    void UpdateActiveFilterList();
    int ExpandAll();
    int ForeachItem(fnItemAction cbAction);
    BOOL ForeachChild(HTREEITEM Parent, fnItemAction cbAction);
    int ClearTreeNodes();
    void AddGroup();
    void BindNodeToFilter(HTREEITEM TreeNode, TFFilter * Filter);
    LRESULT OnLButtonDblClk(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
    HTREEITEM InsertFilter(TFFilter * Filter, HTREEITEM Parent, HTREEITEM After);
};