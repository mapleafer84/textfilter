//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 textfilter.rc 使用
//
#define IDD_ABOUTBOX                    100
#define IDD_FIND                        101
#define IDR_MAINFRAME                   128
#define IDD_FILTER                      201
#define IDR_PANE_TOOLBAR                203
#define IDI_ICON_LAST                   208
#define IDI_ICON2                       209
#define IDI_ICON1                       210
#define IDI_ICON_NEXT                   210
#define IDD_GOTO                        211
#define IDC_EDIT_FILTER_STRING          1000
#define IDC_EDIT_TAG                    1001
#define IDC_CHECK1                      1002
#define IDC_COMBO_COLOR                 1003
#define IDC_COMBO_GROUP                 1005
#define IDC_CHECK2                      1006
#define IDC_COMBO1                      1007
#define IDC_COMBO_KEY                   1007
#define IDC_BUTTON_NEXT                 1008
#define IDC_BUTTON2                     1009
#define IDC_BUTTON_LAST                 1009
#define IDC_BUTTON3                     1010
#define IDC_EDIT1                       1011
#define IDC_EDIT_LINE_NO                1011
#define ID_VIEW_TREEPANE                32774
#define ID_FILE_OPENTFC                 32775
#define ID_FILE_SAVETFC                 32776
#define ID_FILE_SAVETFCAS               32777
#define ID_TFC_OPEN                     32778
#define ID_TFC_SAVE                     32779
#define ID_TFC_SAVEAS                   32780
#define ID_FILE_OPENEDFILEHISTORY       32781
#define ID_FILE_OPENDTFCHISTORY         32782
#define ID_BUTTON32784                  32784
#define ID_BUTTON32785                  32785
#define ID_BUTTON32786                  32786
#define ID_BUTTON32787                  32787

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        213
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
