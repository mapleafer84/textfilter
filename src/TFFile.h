#pragma once
#include <vector>

using namespace std;
#define TALINE_DELETE_FLAG      (1 << 0)
#define TALINE_HILIGHT_FLAG     (1 << 1)


#define TALINE_FLAG_SET(line, flag) ((line)->Flags |= flag)
#define TALINE_FLAG_CLR(line, flag) ((line)->Flags &= ~flag)
#define TALINE_FLAG_CHK(line, flag) ((line)->Flags & flag)

struct TFLineDataInfo {
    TFLineDataInfo()
    {
        Color = 0x0;
    }

    UINT32 LineNo;
    UINT32 Start;
    UINT32 Length;
    UINT32  Color;
};

struct TFLineSet {
    TFLineSet() : m_MaxLineLength(0)
        , m_Data(NULL) {}

    vector<struct TFLineDataInfo *> m_Lines;
    UINT32 m_MaxLineLength;
    LPBYTE m_Data;
};

class TFFile {
public:
    TFFile();

    int LoadFile(TCHAR *FileName);
    int InitLineData();
    struct TFLineSet *GetLineSet();
    LPBYTE GetDataBase();
private:
    HANDLE m_FileHandle;
    HANDLE m_MapHandle;
    UINT32 m_Loaded : 1;
    UINT32 m_Length;
    struct TFLineSet m_LineSet;
public:
    int UnloadFile();
    bool IsLoaded();
};


extern TFFile g_TxtFile;
