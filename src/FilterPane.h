#pragma once
#include "atlctrlx.h"
#define FILTER_PANE_ADDGRP_CMDID     0x1001
#define FILTER_PANE_DELGRP_CMDID     0x1001

class CFilterPane :
    public CPaneContainerImpl<CFilterPane>
{
public:
    DECLARE_WND_CLASS_EX(_T("WTL_PaneContainer"), 0, -1)
    void UpdateLayout(int cxWidth, int cyHeight);
    void UpdateLayout();
    void DrawButtonImage(CDCHandle dc, RECT& rcImage, HPEN hPen);

    BEGIN_MSG_MAP(CFilterPane)
        MESSAGE_HANDLER(WM_CREATE, OnCreate)
        CHAIN_MSG_MAP(CPaneContainerImpl<CFilterPane>)
        REFLECT_NOTIFICATIONS()
    END_MSG_MAP()
    LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
};

