#pragma once
class CFilterDlg : public CDialogImpl<CFilterDlg>
{
private:
    TFFilter *m_Filter;
    CComboBox m_GroupCtrl;
    CColorComboBox m_ColorCtrl;

public:
    enum { IDD = IDD_FILTER };

    BEGIN_MSG_MAP(CFilterDlg)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        COMMAND_HANDLER(IDOK, BN_CLICKED, OnBnClickedOk)
        COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnBnClickedCancel)
    END_MSG_MAP()
    LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
    LRESULT OnBnClickedOk(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    void SetFilter(TFFilter * Filter);
    void InitGroupList();
    void InitItemFromFilter(TFFilter * Filter);
    LRESULT OnBnClickedCancel(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};

