#pragma once
#include "TFFile.h"
#define WM_UPDATE_SCROLL        (WM_USER + 0)
#define WM_UPDATE_SCROLL_POS    (WM_USER + 1)

enum {
    UPDATE_VIEW_CANVAS,
    UPDATE_VIEW_SCROLL,
    UPDATE_VIEW_SCROLL_POS,
};
class TFFilter {
public:
    TFFilter();
    TFFilter(char *NodeName);
    ~TFFilter();
    char *m_NodeName;
    char* m_FilterString;
    int m_FilterStringLength;
    COLORREF m_Color;
    TFFilter *m_Father;
    vector<TFFilter *>m_Children;
    TFLineSet *m_LineSet;
    HTREEITEM m_TreeItem;
    void SetNodeName(TCHAR * NodeName);
    int SetFilterString(TCHAR * FilterString);
    void SetFather(TFFilter * Father);
    void SetColorIndex(int Index);
};

class TFConfig {
public:
    TFConfig();
    ~TFConfig();
    int LoadTfc(TCHAR *FileName);
    void UnloadTfc();
private:
    TCHAR *m_FileName;
    BOOL m_Loaded;
    TFFilter m_RootNode;
public:
    int Initialize(char* Data, DWORD Length);
    TFFilter *InitFilter(char * CfgString, DWORD Length);
    TFFilter * FindFilterNode(TFFilter *FatherNode, char * NodeName);
    char * GetValueFromString(char * String);
    bool IsLoaded();
    TFFilter * GetRootNode();
    void AddFilter(TFFilter * Father, TFFilter * After, TFFilter * NewFilter);
    int SaveTfc(TCHAR * FileName);
    int SaveTfcChildren(HANDLE FileHandle, TFFilter * FatherNode);
    int SetFileName(TCHAR * FileName);
    TCHAR * GetFileName();
};

#define WM_TREEVIEW_NEW_FILTER  (WM_USER + 1)

class TFCtxt
{
public:
    TFCtxt();
    ~TFCtxt();
private:
    int m_FontWidth;
    int m_FontHeight;
    int m_DefaultFontWidth;
    int m_DefaultFontHeight;
    float m_FontRatio;
    HWND m_AttachView;
    TFFile m_File;
    TFConfig m_Config;
    TFLineSet m_ViewLineSet;
    vector<TFFilter *>m_ActiveFilters;
    int m_ViewOffsetX;
    int m_ViewOffsetY;
    int m_HotLine;
    HBRUSH m_BkBrush;
    HBRUSH m_HotLineBrush;
    HPEN m_VLinePen;
    HWND m_TreeView;
    vector<COLORREF> m_ColorSet;
public:
    int GetLineHeight();
    void UpdateFontRatio(bool Inc);
    void AttachView(HWND hWnd);
    HFONT GetFont(void);
    int GetFontWidth();
private:
    void UpdateFont();
    HFONT m_Font;
public:
    int LoadFromFile(TCHAR * FileName);
    int LoadTfc(TCHAR * FileName);
    void UpdateView(UINT32 Target);
    void UpdateViewCanvas(HDC hDc);
    int UpdateViewLineSet();
    bool IsMatchAnyFilter(LPBYTE Data, TFLineDataInfo * LineDataInfo);
    int GetLineCount();
    int GetLineMaxLength();
    int GetLineCountInView();
    int GetViewLineOffset();
    int CalcLineNoWidth();
    void SetViewCharOffset(int Pos);
    void SetViewLineOffset(int Pos, BOOL Update);
    void SetHotLine(int LineNo, BOOL Update);
    int GetFirstLine();
    int GetLineFromY(int Y);
    TFLineDataInfo * GetLineDataInfo(int LineNo);
    LPBYTE GetDatabase();
    TFConfig * GetConfig();
    vector<TFFilter *> *GetActiveFilterList();
    void ClearActiveFilterLineSet();
    int SaveTfc(TCHAR * FileName);
    void NotifyTreeViewNewFilter(TFFilter *NewFilter);
    void SetTreeView(HWND TreeView);
    int FindNextLine(char * szKey, int Length);
    int FindLastLine(char * szKey, int Length);
    void FindReset(void);
private:
    int m_FindStart;
    int m_MatchedLine;
public:
    COLORREF GetColor(int Index);
    int GetColorListSize();
};

extern TFCtxt g_Ctxt;

