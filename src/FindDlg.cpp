#include "stdafx.h"
#include "resource.h"
#include "TFCtxt.h"
#include "FindDlg.h"


BOOL CFindDlg::PreTranslateMessage(MSG* pMsg)
{
    return CWindow::IsDialogMessage(pMsg);
}

LRESULT CFindDlg::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    RECT ctrl_rect;
    RECT rect;
    int btn_width = 0;

    GetClientRect(&rect);

    GetDlgItem(IDC_BUTTON_LAST).GetWindowRect(&ctrl_rect);

    btn_width = ctrl_rect.right - ctrl_rect.left;
    ScreenToClient(&ctrl_rect);
    ctrl_rect.left = rect.right - btn_width - 2;
    ctrl_rect.right = ctrl_rect.left + btn_width;
    GetDlgItem(IDC_BUTTON_LAST).MoveWindow(&ctrl_rect);

    ctrl_rect.left = ctrl_rect.right - 2 * btn_width - 2;
    ctrl_rect.right = ctrl_rect.left + btn_width;
    GetDlgItem(IDC_BUTTON_NEXT).MoveWindow(&ctrl_rect);

    ctrl_rect.left = ctrl_rect.left - 200 - 2;
    ctrl_rect.right = ctrl_rect.left + 200;
    GetDlgItem(IDC_COMBO_KEY).MoveWindow(&ctrl_rect);

    return 0;
}


LRESULT CFindDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    m_cboxKey.Attach(GetDlgItem(IDC_COMBO_KEY));
    m_cboxKey.SetFocus();

    m_btnNext.Attach(GetDlgItem(IDC_BUTTON_NEXT));
    m_btnNext.SetButtonStyle(BS_ICON);
    m_btnNext.SetIcon((HICON)::LoadImage(ModuleHelper::GetResourceInstance(),
        MAKEINTRESOURCE(IDI_ICON_NEXT),
        IMAGE_ICON,
        GetSystemMetrics(SM_CXSMICON),
        GetSystemMetrics(SM_CYSMICON),
        0));

    m_btnLast.Attach(GetDlgItem(IDC_BUTTON_LAST));
    m_btnLast.SetButtonStyle(BS_ICON);
    m_btnLast.SetIcon((HICON)::LoadImage(ModuleHelper::GetResourceInstance(),
        MAKEINTRESOURCE(IDI_ICON_LAST),
        IMAGE_ICON,
        GetSystemMetrics(SM_CXSMICON),
        GetSystemMetrics(SM_CYSMICON),
        0));

    return 0;
}

LRESULT CFindDlg::OnBtnNext(WORD, WORD, HWND, BOOL &)
{
    char *szKey = NULL;
    int Length = GetWindowTextLengthA(GetDlgItem(IDC_COMBO_KEY));

    if (Length != 0) {
        szKey = new char[Length + 1];
        ::GetWindowTextA(GetDlgItem(IDC_COMBO_KEY), szKey, Length + 1);

        szKey[Length] = 0;

        g_Ctxt.FindNextLine(szKey, Length);
        LogHistory(szKey);

        if (szKey != NULL) {
            delete[] szKey;
        }
    }

    return LRESULT();
}

LRESULT CFindDlg::OnBtnLast(WORD, WORD, HWND, BOOL &)
{
    char *szKey = NULL;
    int Length = GetWindowTextLengthA(GetDlgItem(IDC_COMBO_KEY));

    if (Length != 0) {
        szKey = new char[Length + 1];
        ::GetWindowTextA(GetDlgItem(IDC_COMBO_KEY), szKey, Length + 1);

        szKey[Length] = 0;

        g_Ctxt.FindLastLine(szKey, Length);
        LogHistory(szKey);

        if (szKey != NULL) {
            delete[] szKey;
        }
    }
    return LRESULT();
}


void CFindDlg::LogHistory(char * szKey)
{
    if (!IsLogHistoryExist(szKey)) {
        TCHAR *wszKey = NULL;
        int wszKeyLen = MultiByteToWideChar(CP_ACP, 0, szKey, strlen(szKey), wszKey, 0);
        wszKey = new TCHAR[wszKeyLen + 1];
        MultiByteToWideChar(CP_ACP, 0, szKey, strlen(szKey), wszKey, wszKeyLen);
        wszKey[wszKeyLen] = 0;
        m_cboxKey.AddString(wszKey);
        delete[] wszKey;
    }
}


bool CFindDlg::IsLogHistoryExist(char * szKey)
{
    for (int i = 0; i < m_cboxKey.GetCount(); i++) {
        int Length = (int)::SendMessageA(m_cboxKey.m_hWnd, CB_GETLBTEXTLEN, i, 0L);
        char *ItemText = new char[Length + 1];
        ::SendMessageA(m_cboxKey.m_hWnd, CB_GETLBTEXT, i, (LPARAM)ItemText);
        ItemText[Length] = 0;
        if (strcmp(szKey, ItemText) == 0) {
            delete[] ItemText;
            return true;
        }
        delete[] ItemText;
    }
    return false;
}
