#include "stdafx.h"
#include "resource.h"
#include "GotoDlg.h"
#include "TFCtxt.h"

LRESULT CGotoDlg::OnInitDialog(UINT, WPARAM, LPARAM, BOOL &)
{
    CenterWindow();
    GetDlgItem(IDC_EDIT_LINE_NO).SetFocus();
    return LRESULT();
}

LRESULT CGotoDlg::OnCloseCmd(WORD, WORD wID, HWND, BOOL &)
{
    EndDialog(IDCANCEL);
    return LRESULT();
}

LRESULT CGotoDlg::OnGotoCmd(WORD, WORD wID, HWND, BOOL &)
{
    TCHAR *szLineNo = NULL;
    int Length = GetDlgItem(IDC_EDIT_LINE_NO).GetWindowTextLength();
    if (Length > 0) {
        int LineNo = 0;
        szLineNo = new TCHAR[Length + 1];
        GetDlgItem(IDC_EDIT_LINE_NO).GetWindowText(szLineNo, Length + 1);
        szLineNo[Length] = 0;
        LineNo = _wtoi(szLineNo);
        delete[] szLineNo;

        g_Ctxt.SetHotLine(LineNo - 1, FALSE);
        g_Ctxt.SetViewLineOffset(LineNo - 1, TRUE);

        EndDialog(IDOK);
    }
    return LRESULT();
}
