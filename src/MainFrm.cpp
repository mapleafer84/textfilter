﻿// MainFrm.cpp : implmentation of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "aboutdlg.h"
#include "FindDlg.h"
#include "GotoDlg.h"
#include "textfilterView.h"
#include "FilterTreeView.h"
#include "FilterPane.h"
#include "MainFrm.h"
#include "TFCtxt.h"
#include <cstringt.h>

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	if(CFrameWindowImpl<CMainFrame>::PreTranslateMessage(pMsg))
		return TRUE;

	return m_view.PreTranslateMessage(pMsg);
}

BOOL CMainFrame::OnIdle()
{
	UIUpdateToolBar();
	return FALSE;
}

void CMainFrame::UpdateLayout(BOOL bResizeBars)
{
    RECT rect = { 0 };
    GetClientRect(&rect);

    // position bars and offset their dimensions
    UpdateBarsPosition(rect, bResizeBars);

    if (m_IsFindViewShow && m_findview.IsWindow()) {
        m_findview.SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, 20, 0);
        rect.top += 20;
    }

    // resize client window
    if (m_hWndClient != NULL)
        ::SetWindowPos(m_hWndClient, NULL, rect.left, rect.top,
            rect.right - rect.left, rect.bottom - rect.top,
            SWP_NOZORDER | SWP_NOACTIVATE);
}
LRESULT CMainFrame::OnHotkey(UINT, WPARAM wParam, LPARAM lParam, BOOL &)
{
    if (HIWORD(lParam) == 0x46) {
        //Ctrl + F
        m_IsFindViewShow = !m_IsFindViewShow;

        UpdateLayout();

        if (m_IsFindViewShow) {
            m_findview.ShowWindow(SW_SHOW);
        }
        else {
            m_findview.ShowWindow(SW_HIDE);
            g_Ctxt.FindReset();
        }
    }
    else if (HIWORD(lParam) == 0x47) {
        //Ctrl + G
        CGotoDlg dlgGoto;
        dlgGoto.DoModal();
    }

    return 0;
}

LRESULT CMainFrame::OnGroupAdd(WORD, WORD, HWND, BOOL &)
{
    m_treeview.AddGroup();
    return LRESULT();
}

LRESULT CMainFrame::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	CreateSimpleToolBar();

	CreateSimpleStatusBar();

	m_hWndClient = m_splitter.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	//m_pane.SetPaneContainerExtendedStyle(PANECNT_GRADIENT | PANECNT_NOCLOSEBUTTON);
	m_pane.Create(m_splitter, _T("Filters"), WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

    m_treeview.Create(m_pane, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_EDITLABELS | TVS_CHECKBOXES | TVS_SHOWSELALWAYS, WS_EX_CLIENTEDGE);
	m_pane.SetClient(m_treeview);
    g_Ctxt.SetTreeView(m_treeview);

	m_view.Create(m_splitter, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_HSCROLL | WS_VSCROLL, 0);

    // replace with appropriate values for the app
    
    g_Ctxt.AttachView(m_view.m_hWnd);

	m_splitter.SetSplitterPanes(m_pane, m_view);
	UpdateLayout();
	m_splitter.SetSplitterPosPct(25);

    m_findview.Create(m_hWnd);
    m_IsFindViewShow = FALSE;

	UIAddToolBar(m_hWndToolBar);
	UISetCheck(ID_VIEW_TOOLBAR, 1);
	UISetCheck(ID_VIEW_STATUS_BAR, 1);
	UISetCheck(ID_VIEW_TREEPANE, 1);

    RegisterHotKey(m_hWnd, 1, MOD_CONTROL | MOD_NOREPEAT, 0x46);
    RegisterHotKey(m_hWnd, 1, MOD_CONTROL | MOD_NOREPEAT, 0x47);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	return 0;
}

LRESULT CMainFrame::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	// unregister message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);
	pLoop->RemoveIdleHandler(this);

	bHandled = FALSE;
	return 1;
}

LRESULT CMainFrame::OnFileExit(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	PostMessage(WM_CLOSE);
	return 0;
}

LRESULT CMainFrame::OnFileOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: add code to initialize document
    CFileDialog dlgOpen(TRUE, 0, 0, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST
        , _T("Txt file\0*.TXT\0Log file\0*.LOG\0"));
    if (dlgOpen.DoModal() == IDOK) {
        if (g_Ctxt.LoadFromFile(dlgOpen.m_szFileName) == 0) {
        }
    }
	return 0;
}

LRESULT CMainFrame::OnViewToolBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndToolBar);
	::ShowWindow(m_hWndToolBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_TOOLBAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnViewStatusBar(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	BOOL bVisible = !::IsWindowVisible(m_hWndStatusBar);
	::ShowWindow(m_hWndStatusBar, bVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
	UISetCheck(ID_VIEW_STATUS_BAR, bVisible);
	UpdateLayout();
	return 0;
}

LRESULT CMainFrame::OnAppAbout(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CAboutDlg dlg;
	dlg.DoModal();
	return 0;
}

LRESULT CMainFrame::OnViewTreePane(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	bool bShow = (m_splitter.GetSinglePaneMode() != SPLIT_PANE_NONE);
	m_splitter.SetSinglePaneMode(bShow ? SPLIT_PANE_NONE : SPLIT_PANE_RIGHT);
	UISetCheck(ID_VIEW_TREEPANE, bShow);

	return 0;
}

LRESULT CMainFrame::OnTreePaneClose(WORD /*wNotifyCode*/, WORD /*wID*/, HWND hWndCtl, BOOL& /*bHandled*/)
{
	if(hWndCtl == m_pane.m_hWnd)
	{
		m_splitter.SetSinglePaneMode(SPLIT_PANE_RIGHT);
		UISetCheck(ID_VIEW_TREEPANE, 0);
	}

	return 0;
}

LRESULT CMainFrame::OnTfcOpen(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    CFileDialog dlgOpen(TRUE, 0, 0, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_PATHMUSTEXIST
        , _T("Filters config\0*.tfc\0"));
    if (dlgOpen.DoModal() == IDOK) {

        ClearTreeView();

        if (g_Ctxt.LoadTfc(dlgOpen.m_szFileName) == 0) {
            InitTreeView();
            m_treeview.ExpandAll();
        }
    }

    return 0;
}
LRESULT CMainFrame::OnTfcSave(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    if (g_Ctxt.GetConfig()->GetFileName() != NULL) {
        g_Ctxt.SaveTfc(g_Ctxt.GetConfig()->GetFileName());
    }
    else {
        BOOL bHandle = FALSE;
        OnTfcSaveAs(0, 0, 0, bHandle);
    }
    return 0;

}
LRESULT CMainFrame::OnTfcSaveAs(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    CFileDialog dlgOpen(FALSE, 0, 0, OFN_HIDEREADONLY | OFN_PATHMUSTEXIST
        , _T("Filters config\0*.tfc\0"));
    if (dlgOpen.DoModal()) {
        g_Ctxt.SaveTfc(dlgOpen.m_szFileName);
    }
    return 0;
}

LRESULT CMainFrame::OnEndEditGroup(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
    LPNMTVDISPINFO tvinfo = (LPNMTVDISPINFO)pnmh;
    TFFilter *Filter = NULL;

    if ((tvinfo->item.hItem != NULL)
        && (tvinfo->item.pszText != NULL)
        && (wcslen(tvinfo->item.pszText) > 0)) {
        Filter = (TFFilter *)m_treeview.GetItemData(tvinfo->item.hItem);
        if (Filter != NULL) {
            Filter->SetNodeName(tvinfo->item.pszText);
            m_treeview.SetItemText(tvinfo->item.hItem, tvinfo->item.pszText);
        }
    }
    return LRESULT();
}

LRESULT CMainFrame::OnCopy(WORD, WORD, HWND, BOOL &)
{
    int Length = 0;
    HANDLE hGlobalMemory = NULL;
    LPBYTE lpGlobalMemory = NULL;
    int HtmlFmt = RegisterClipboardFormat(TEXT("HTML Format"));

    Length = PrepareHtlmData(NULL, 0);

    hGlobalMemory = GlobalAlloc(GHND, Length + 1);
    lpGlobalMemory = (LPBYTE)GlobalLock(hGlobalMemory);

    PrepareHtlmData((char *)lpGlobalMemory, Length);

    GlobalUnlock(hGlobalMemory);
    ::OpenClipboard(this->m_hWnd);
    ::EmptyClipboard();
    ::SetClipboardData(HtmlFmt, hGlobalMemory);
    ::CloseClipboard();

    return 0;
}


int CMainFrame::InitTreeView()
{
    TFConfig *Config = g_Ctxt.GetConfig();
     
    if (Config->IsLoaded()) {
        TFFilter *FatherNode = Config->GetRootNode();
        InitTreeNode(NULL, FatherNode);
    }
    return 0;
}


HTREEITEM CMainFrame::InitTreeNode(HTREEITEM FatherTreeNode, TFFilter * FatherFilterNode)
{
    HTREEITEM TreeNode;
    TFFilter *FilterNode;

    if (FatherFilterNode != NULL) {
        for (int i = 0; i < FatherFilterNode->m_Children.size(); i++) {
            FilterNode = FatherFilterNode->m_Children.at(i);
            TreeNode = m_treeview.InsertFilter(FilterNode, FatherTreeNode, NULL);
            InitTreeNode(TreeNode, FilterNode);
        }
    }
    return HTREEITEM();
}


int CMainFrame::ClearTreeView()
{
    return m_treeview.ClearTreeNodes();
}

#define HTML_CRLF   "\r\n"
#define HTML_TEMPLATE_HEADER "Version : 0.9" HTML_CRLF\
        "StartHTML : -1" HTML_CRLF \
        "EndHTML : -1"  HTML_CRLF \
        "StartFragment : 000080" HTML_CRLF \
        "EndFragment : ******" HTML_CRLF \

int CMainFrame::PrepareHtlmData(char * ClipbrdData, int Length)
{
    int HtmlLen = CalcHtmlData();
    char *TempPos = NULL;
    char EndFragmentValue[7];

    if (ClipbrdData == NULL || Length < HtmlLen) {
        return HtmlLen;
    }

    strcpy(ClipbrdData, HTML_TEMPLATE_HEADER);
    TempPos = strstr(ClipbrdData, "******");
    sprintf(EndFragmentValue, "%06d", HtmlLen);
    memcpy(TempPos, EndFragmentValue, 6);

    TempPos = ClipbrdData + strlen(ClipbrdData);
    strcpy(TempPos, "<font color = \"#FF0000\">hello word!</font>");
    return 0;
}


int CMainFrame::CalcHtmlData()
{
    int HtmlHeaderLen = strlen(HTML_TEMPLATE_HEADER);
    int HtlmDataLen = strlen("<font color=\"#FF0000\">hello word!</font>");

    return HtmlHeaderLen + HtlmDataLen;
}
