cmake_minimum_required(VERSION 2.8)
project (texthelper)

add_definitions(-DUNICODE -D_UNICODE)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_CURRENT_BINARY_DIR}\\..\\src\\texthelper.res")
include_directories(C:\\WTL90_4140_Final\\Include)
include_directories("C:\\Program Files\\Microsoft SDK\\include\\atl\\include")
LINK_DIRECTORIES("C:\\Program Files\\Microsoft SDK\\include\\atl\\lib")




aux_source_directory(src DIR_SRCS)
add_executable(texthelper ${DIR_SRCS})

add_custom_command(OUTPUT texthelper.res PRE_LINK
		COMMAND rc.exe /iC:\\WTL90_4140_Final\\Include ../src/texthelper.rc
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
) 

set_target_properties(${PROJECT_NAME} PROPERTIES LINK_FLAGS /SUBSYSTEM:WINDOWS)